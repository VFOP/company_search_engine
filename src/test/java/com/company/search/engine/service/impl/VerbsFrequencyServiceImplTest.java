package com.company.search.engine.service.impl;

import com.company.search.engine.model._Tweet;
import com.company.search.engine.service.SearchQueryBuilderService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import java.util.*;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

/**
 * Created by flooprea on 8/22/2018
 */

@RunWith(MockitoJUnitRunner.class)
public class VerbsFrequencyServiceImplTest {

    @Mock
    private SearchQueryBuilderService searchQueryBuilderService;

    @InjectMocks
    private VerbsFrequencyServiceImpl verbsFrequencyService;

    @Before
    public void setUp() {
        List<_Tweet> tweets = getTweetList();
        when(searchQueryBuilderService.findTweetsByTerm(anyString())).thenReturn(tweets);
    }

    @Test
    public void testGetMostFrequentVerbsAfterPronoun() {
        Map<String, Integer> expectedMap = new LinkedHashMap<>();
        expectedMap.put("received", 1);
        expectedMap.put("are making", 2);
        expectedMap.put("could test", 1);


        Map<String, Map<String, Integer>> map = verbsFrequencyService.getMostFrequentVerbsAfterPronoun("we");
        assertEquals(expectedMap, map);
    }

    private List<_Tweet> getTweetList() {
        List<_Tweet> tweets = new ArrayList<>();

        _Tweet firstTweet = new _Tweet();
        firstTweet.setText("We received congrats in the first tweet!");
        firstTweet.setFromUser("florinoprea93");
        firstTweet.setCreatedAt(new Date(2018, 03, 10));
        firstTweet.setId("1");
        firstTweet.setTwitterProfileId("1");

        _Tweet secondTweet = new _Tweet();
        secondTweet.setText("We are making an announce in the second tweet!");
        secondTweet.setFromUser("coprea94");
        secondTweet.setCreatedAt(new Date(2018, 07, 12));
        secondTweet.setId("2");
        secondTweet.setTwitterProfileId("2");

        _Tweet thirdTweet = new _Tweet();
        thirdTweet.setText("We could test this method!");
        thirdTweet.setFromUser("andra04");
        thirdTweet.setCreatedAt(new Date(2015, 05, 16));
        thirdTweet.setId("3");
        thirdTweet.setTwitterProfileId("3");

        _Tweet fourthTweet = new _Tweet();
        fourthTweet.setText("We are making an announce in the fourth tweet!");
        fourthTweet.setFromUser("coprrea94");
        fourthTweet.setCreatedAt(new Date(2018, 01, 12));
        fourthTweet.setId("4");
        fourthTweet.setTwitterProfileId("4");

        tweets.add(firstTweet);
        tweets.add(fourthTweet);
        tweets.add(thirdTweet);
        tweets.add(fourthTweet);

        return tweets;
    }

    private List<String> getTaggedWordList() {
        List<String> taggedWordList = new ArrayList<>();
        taggedWordList.add("We_PRP");
        taggedWordList.add("received_VBD");
        taggedWordList.add("congrats_NNS");
        taggedWordList.add("in_IN");
        taggedWordList.add("the_DT");
        taggedWordList.add("first_JJ");
        taggedWordList.add("tweet_NN");
        taggedWordList.add("!_.");

        return taggedWordList;
    }

    @After
    public void destroy() {

    }
}
