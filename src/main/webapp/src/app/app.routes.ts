import {Router, Routes} from "@angular/router";
import {HomeComponent} from "./home/home.component";
import {CompanyListComponent} from "./company/company-list.component";
import {TwitterProfileListComponent} from "./twitter-profile/twitter-profile-list.component";
import {TweetListComponent} from "./tweet/tweet-list.component";
import {CompanyListResolver} from "./company/service/company-list.resolver";
import {TwitterProfileListResolver} from "./twitter-profile/service/twitter-profile-list.resolver";
import {TweetListResolver} from "./tweet/service/tweet-list.resolver";
import {EventListComponent} from "./event/event-list.component";
import {EventListResolver} from "./event/service/event-list.resolver";
import {CompanyTypePerCountryComponent} from "./stats/company-type-per-country/company-type-per-country.component";
import {CompanyTypePerCountryResolver} from "./stats/company-type-per-country/service/company-type-per-country.resolver";

export const appRoutes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'company-type-per-country',
    component: CompanyTypePerCountryComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      companyTypePerCountry: CompanyTypePerCountryResolver
    }
  },
  {
    path: 'company',
    component: CompanyListComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      company: CompanyListResolver
    }
  },
  {
    path: 'twitter-profile',
    component: TwitterProfileListComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      twitterProfile: TwitterProfileListResolver
    }
  },
  {
    path: 'tweet',
    component: TweetListComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      tweet: TweetListResolver
    }
  },
  {
    path: 'event',
    component: EventListComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      event: EventListResolver
    }
  },

  {
    path: 'event/:eventName',
    component: EventListComponent,
    runGuardsAndResolvers: 'always',
    resolve: {
      event: EventListResolver,
    }
  },

  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }
]
