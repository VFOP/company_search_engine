import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {ITweet} from "../../tweet/model/tweet.model";
import {Observable} from "rxjs/Observable";
import {IEventModel} from "../../event/event.model";

@Injectable()
export class SearchService {
  private url = 'http://localhost:8081/api/tweet/events/';

  constructor(private http: HttpClient) {
  }

  getEventResults(eventName: string, pageNumber: number): Observable<IEventModel[]> {
    return this.http.get<IEventModel[]>(this.url + eventName + '?page=' + pageNumber);
  }


}
