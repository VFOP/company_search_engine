import {Injectable, OnInit} from "@angular/core";
import * as _ from 'underscore';
import {IPager} from "./pager.model";

@Injectable()
export class PagerService {

  static getPager(totalItems: number, currentPage: number, pageSize: number): IPager {
    let startPage: number;
    let endPage: number;

    let totalPages = Math.ceil(totalItems / pageSize);

    if (totalPages <= 5) {
      startPage = 1;
      endPage = totalPages;
    } else {
      if (currentPage <= 3) {
        startPage = 1;
        endPage = 5;
      } else if (currentPage + 1 >= totalPages) {
        startPage = totalPages - 4;
        endPage = totalPages;
      } else {
        startPage = currentPage - 2;
        endPage = currentPage + 2;
      }
    }

    let pages = _.range(startPage, endPage + 1);
    return {
      startPage: startPage,
      endPage: endPage,
      currentPage: currentPage,
      pageSize: pageSize,
      pages: pages,
      totalItems: totalItems,
      totalPages: totalPages
    }
  }
}


