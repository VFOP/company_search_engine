
export interface IPager {
  startPage: number;
  endPage: number;
  currentPage: number;
  pageSize: number;
  pages: number[];
  totalItems: number;
  totalPages: number
}
