
export class ProcessString {

  static processUrl(url: string) {
    let indexPage = url.indexOf('?page');

    return indexPage > 0 ? decodeURI(url.slice(0, indexPage)) : decodeURI(url);
  }
}
