import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {ITweet} from "../model/tweet.model";
import {TweetService} from "./tweet.service";
import {Injectable} from "@angular/core";

@Injectable()
export class TweetListResolver implements Resolve<ITweet[]> {

  constructor(private tweetService: TweetService) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    let page = route.queryParams['page'];
    return page ? this.tweetService.getTweets(page) : this.tweetService.getTweets(0);
  }

}
