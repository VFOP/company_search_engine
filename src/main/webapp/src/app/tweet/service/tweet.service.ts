import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Observable";
import {ITweet} from "../model/tweet.model";
import {HttpClient} from "@angular/common/http";

@Injectable()
export class TweetService {

  private url = 'http://localhost:8081/api/tweet/tweets';

  constructor(private http: HttpClient) {
  }

  getTweets(page: number): Observable<ITweet[]> {
    return this.http.get<ITweet[]>(this.url + '?page=' + page);
  }

}
