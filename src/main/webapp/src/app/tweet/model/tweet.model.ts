
export interface ITweet {
  id: string;
  twitterProfileId: string;
  text: string;
  createdAt: string;
  fromUser: string;
}
