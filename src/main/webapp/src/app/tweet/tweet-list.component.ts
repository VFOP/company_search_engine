import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {ITweet} from "./model/tweet.model";
import {PagerService} from "../utils/pager/pager.service";
import {IPager} from "../utils/pager/pager.model";

@Component({
  selector: 'tweet',
  templateUrl: 'tweet-list.component.html',
  styleUrls: ['./style/tweet.css']
})
export class TweetListComponent implements OnInit {

  private tweets: ITweet[];
  private pager: IPager;

  constructor(private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.setPage(1);
  }

  setPage(page: number) {
    this.router.navigate(['/tweet'], {queryParams: {page: page}});
    this.getTweets(page);
  }

  getTweets(page: number) {
    let data = this.route.snapshot.data['tweet'];
    this.tweets = data['content'];
    let totalItems = data['totalElements'];
    let pageSize = data['numberOfElements'];
    this.pager = PagerService.getPager(totalItems, page, pageSize);
  }

}
