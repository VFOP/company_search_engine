import {Component} from "@angular/core";
import {Input} from "@angular/core";
import {ITweet} from "./model/tweet.model";

@Component({
  selector: '<tweet-thumbnail>',
  templateUrl: 'tweet-thumbnail.component.html',
  styleUrls: ['./style/tweet.css']
})
export class TweetThumbnailComponent {
  @Input() tweet: ITweet;
}
