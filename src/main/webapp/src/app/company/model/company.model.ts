export interface ICompany {
  id: string;
  name: string;
  uri: string;
  employeeNumber: number;
  type: string;
  country: string;
  events: any[];
}
