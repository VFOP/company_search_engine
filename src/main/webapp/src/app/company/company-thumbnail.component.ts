import {Component,  Input} from "@angular/core";
import {ICompany} from "./model/company.model";

@Component({
  selector: 'company-thumbnail',
  templateUrl: 'company-thumbnail.component.html',
  styleUrls: ['./style/company.css']
})
export class CompanyThumbnailComponent {
  @Input() company: ICompany;
}
