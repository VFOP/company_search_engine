import {ICompany} from "../model/company.model";
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {CompanyService} from "./company.service";
import {Injectable} from "@angular/core";

@Injectable()
export class CompanyListResolver implements Resolve<ICompany[]> {

  constructor(private companyService: CompanyService) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    let page = route.queryParams['page'];
    return page ? this.companyService.getCompanies(page) : this.companyService.getCompanies(0);
  }

}
