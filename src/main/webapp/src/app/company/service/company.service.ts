import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {ICompany} from "../model/company.model";

@Injectable()
export class CompanyService {
  private url = "http://localhost:8081/api/company/companies";


  constructor(private http: HttpClient) {
  }

  getCompanies(page: number): Observable<ICompany[]> {
    return this.http.get<ICompany[]>(this.url + '?page=' + page);
  }
}
