import {Component, OnInit} from "@angular/core";
import {ICompany} from "./model/company.model";
import {ActivatedRoute, Router} from "@angular/router";
import {PagerService} from "../utils/pager/pager.service";
import {IPager} from "../utils/pager/pager.model";

@Component({
  selector: 'company',
  templateUrl: 'company-list.component.html',
  styleUrls: ['./style/company.css']
})
export class CompanyListComponent implements OnInit {

  private companies: ICompany[];
  private pager: IPager;

  constructor(private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.setPage(1);
  }

  getCompanies(page: number) {
    debugger;
    let data = this.route.snapshot.data['company'];
    this.companies = data['content'];
    let totalItems = data['totalElements'];
    let pageSize = data['numberOfElements'];
    this.pager = PagerService.getPager(totalItems, page, pageSize);
  }

  setPage(page: number) {
    this.router.navigate(['/company'], {queryParams: {page: page}});
    this.getCompanies(page);
  }

}
