import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';


import {AppComponent} from './app.component';
import {NavBarComponent} from "./utils/nav/navbar.component";
import {CompanyListComponent} from "./company/company-list.component";
import {TweetListComponent} from "./tweet/tweet-list.component";
import {TwitterProfileListComponent} from "./twitter-profile/twitter-profile-list.component";
import {HomeComponent} from "./home/home.component";
import {RouterModule} from "@angular/router";
import {appRoutes} from "./app.routes";
import {CommonModule} from "@angular/common";
import {CompanyService} from "./company/service/company.service";
import {CompanyThumbnailComponent} from "./company/company-thumbnail.component";
import {PagerService} from "./utils/pager/pager.service";
import {HttpClientModule} from "@angular/common/http";
import {HttpModule} from "@angular/http";
import {CompanyListResolver} from "./company/service/company-list.resolver";
import {TwitterProfileService} from "./twitter-profile/service/twitter-profile.service";
import {TwitterProfileListResolver} from "./twitter-profile/service/twitter-profile-list.resolver";
import {TwitterProfileThumbnailComponent} from "./twitter-profile/twitter-profile-thumbnail.component";
import {TweetThumbnailComponent} from "./tweet/tweet-thumbnail.component";
import {TweetService} from "./tweet/service/tweet.service";
import {TweetListResolver} from "./tweet/service/tweet-list.resolver";
import {FormsModule} from "@angular/forms";
import {SearchService} from "./utils/search/search.service";
import {EventListResolver} from "./event/service/event-list.resolver";
import {EventListComponent} from "./event/event-list.component";
import {EventThumbnailComponent} from "./event/event-thumbnail.component";
import {ChartsModule} from "ng2-charts";
import {CompanyTypePerCountryComponent} from "./stats/company-type-per-country/company-type-per-country.component";
import {StatsService} from "./stats/stats.service";
import {CompanyTypePerCountryResolver} from "./stats/company-type-per-country/service/company-type-per-country.resolver";
import {BsDropdownModule} from "ngx-bootstrap";


@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    HomeComponent,
    CompanyListComponent,
    TwitterProfileListComponent,
    CompanyThumbnailComponent,
    TwitterProfileThumbnailComponent,
    TweetListComponent,
    TweetThumbnailComponent,
    EventListComponent,
    EventThumbnailComponent,
    CompanyTypePerCountryComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    ChartsModule,
    FormsModule,
    BsDropdownModule.forRoot(),
    HttpModule,
    RouterModule.forRoot(appRoutes, {useHash: true})
  ],
  providers: [
    HttpClientModule,
    PagerService,
    CompanyService,
    CompanyListResolver,
    TwitterProfileService,
    TwitterProfileListResolver,
    TweetService,
    TweetListResolver,
    SearchService,
    EventListResolver,
    StatsService,
    CompanyTypePerCountryResolver

  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
