import {Component, Input} from "@angular/core";
import {ITwitterProfile} from "./model/twitter-profile.model";

@Component({
  selector: 'twitter-profile-thumbnail',
  templateUrl: 'twitter-profile-thumbnail.component.html',
  styleUrls: ['./style/twitter-profile.css']
})
export class TwitterProfileThumbnailComponent {
  @Input() twitterProfile: ITwitterProfile;
}
