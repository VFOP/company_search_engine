export interface ITwitterProfile {
  id: string;
  companyId: string;
  screenName: string;
  name: string;
  description: string;
  verified: boolean;
}
