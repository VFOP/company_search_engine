import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {ITwitterProfile} from "../model/twitter-profile.model";

@Injectable()
export class TwitterProfileService {
  private url = "http://localhost:8081/api/twitterProfile/twitterProfiles";

  constructor(private http: HttpClient) {
  }

  getTwitterProfiles(page: number): Observable<ITwitterProfile[]> {
    return this.http.get<ITwitterProfile[]>(this.url + '?page=' + page);

  }
}
