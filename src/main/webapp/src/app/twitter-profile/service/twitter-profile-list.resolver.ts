import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {ITwitterProfile} from "../model/twitter-profile.model";
import {TwitterProfileService} from "./twitter-profile.service";

@Injectable()
export class TwitterProfileListResolver implements Resolve<ITwitterProfile[]> {

  constructor(private twitterProfileService: TwitterProfileService) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    let page = route.queryParams['page'];

    return page ? this.twitterProfileService.getTwitterProfiles(page) : this.twitterProfileService.getTwitterProfiles(0);
  }

}
