import {Component, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {ITwitterProfile} from "./model/twitter-profile.model";
import {IPager} from "../utils/pager/pager.model";
import {PagerService} from "../utils/pager/pager.service";

@Component({
  selector: 'twitter-profile',
  templateUrl: 'twitter-profile-list.component.html',
  styleUrls: ['./style/twitter-profile.css']
})
export class TwitterProfileListComponent implements OnInit {

  private twitterProfiles: ITwitterProfile[];
  private pager: IPager;

  constructor(private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.setPage(1);
  }

  setPage(page: number) {
    this.router.navigate(['/twitter-profile'], {queryParams: {page: page}});
    this.getTwitterProfiles(page);
  }

  private getTwitterProfiles(page: number) {
    let data = this.route.snapshot.data['twitterProfile'];
    this.twitterProfiles = data['content'];
    let totalItems = data['totalElements'];
    let pageSize = data['numberOfElements'];
    this.pager = PagerService.getPager(totalItems, page, pageSize);
  }
}
