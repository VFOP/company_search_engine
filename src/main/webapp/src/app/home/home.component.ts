import {Component, OnInit} from "@angular/core";
import {Input} from "@angular/compiler/src/core";
import {SearchService} from "../utils/search/search.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'home',
  templateUrl: 'home.component.html',
  styleUrls: ['home.component.css']
})
export class HomeComponent implements OnInit {
  wordSearch: string;

  constructor(private searchService: SearchService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.wordSearch = "";
  }

  search() {
    this.router.navigate(['/event', this.wordSearch]);
  }

}
