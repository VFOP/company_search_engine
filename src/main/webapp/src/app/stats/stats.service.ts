import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";


@Injectable()
export class StatsService {

  private url = "http://localhost:8081/api/statistic/companyTypePerCountry";


  constructor(private http: HttpClient) {

  }

  getCompanyTypePerCountryStats(): Observable<any[]> {
    return this.http.get<any[]>(this.url);

  }
}
