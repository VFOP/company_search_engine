import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve} from "@angular/router";
import {StatsService} from "../../stats.service";

@Injectable()
export class CompanyTypePerCountryResolver implements Resolve<any[]>{

  constructor(private statsService: StatsService) {
  }

  resolve(route: ActivatedRouteSnapshot) {
    return this.statsService.getCompanyTypePerCountryStats();
  }
}
