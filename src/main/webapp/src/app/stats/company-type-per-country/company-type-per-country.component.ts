import {Component, OnInit} from "@angular/core";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'company-type-per-country',
  styleUrls: ['./styles/company-type-per-country.component.css'],
  templateUrl: './company-type-per-country.component.html'
})
export class CompanyTypePerCountryComponent implements OnInit {

  private data: string[];

  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[];
  public barChartType: string = 'line';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [];

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.data = this.route.snapshot.data['companyTypePerCountry'];
    this.barChartLabels = Object.keys(this.data['PUBLIC']);
    let publicData = Object.values(this.data['PUBLIC']);
    let privateData = Object.values(this.data['PRIVATE']);
    let publicLimitedData = Object.values(this.data['PUBLIC_LIMITED']);

    this.barChartData.push({data: publicData, label: 'Public Company'});
    this.barChartData.push({data: privateData, label: 'Private Company'});
    this.barChartData.push({data: publicLimitedData, label: 'Public Limited Company'});
  }
}
