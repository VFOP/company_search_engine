import {Component, OnInit} from "@angular/core";
import {IEventModel} from "./event.model";
import {IPager} from "../utils/pager/pager.model";
import {ActivatedRoute, Router} from "@angular/router";
import {PagerService} from "../utils/pager/pager.service";
import {ProcessString} from "../utils/process/process-string";

@Component({
  selector: 'event-list',
  templateUrl: 'event-list.component.html',
  styleUrls: ['./style/event.css']

})
export class EventListComponent implements OnInit {

  private events: IEventModel[];
  private pager: IPager;

  constructor(private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
    this.setPage(1);
  }


  private setPage(page: number) {
    this.router.navigate([ProcessString.processUrl(this.router.url)], {queryParams: {page: page}});
    this.getEvents(page);
  }

  getEvents(page: number) {
    let data = this.route.snapshot.data['event'];
    if (data) {  //TODO correct the server pagination
      this.events = data['content'];
      let totalItems = data['totalElements'];
      let pageSize = data['numberOfElements'];

      if (page == 1 && data['size'] > this.events.length) {
        totalItems = this.events.length;
      }

      this.pager = PagerService.getPager(totalItems, page, pageSize);
    }
  }


}
