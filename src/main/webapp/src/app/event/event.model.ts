
export interface IEventModel {
  text: string;
  createdAt: string;
  createdBy: string
}
