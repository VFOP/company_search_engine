import {Component, Input} from "@angular/core";
import {IEventModel} from "./event.model";

@Component({
  selector:'<event-thumbnail',
  templateUrl: 'event-thumbnail.component.html',
  styleUrls: ['./style/event.css']

})
export class EventThumbnailComponent {
  @Input() event: IEventModel
}
