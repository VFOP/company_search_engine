import {Injectable} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, Router} from "@angular/router";
import {IEventModel} from "../event.model";
import {SearchService} from "../../utils/search/search.service";


@Injectable()
export class EventListResolver implements Resolve<IEventModel[]> {


  constructor(private searchService: SearchService) {
  }

  resolve(route: ActivatedRouteSnapshot) {1
    let page = route.queryParams['page'];
    let eventName = route.params['eventName'];
    debugger;
    return eventName ? this.searchService.getEventResults(decodeURI(eventName), page ? page : 1) : this.searchService.getEventResults('', 1);
  }

}
