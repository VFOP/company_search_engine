package com.company.search.engine.config;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by flooprea on 3/24/2018
 */
@Configuration
public class ElasticsearchConfiguration implements FactoryBean<TransportClient>, InitializingBean, DisposableBean {

    private static final Logger logger = LoggerFactory.getLogger(ElasticsearchConfiguration.class);
    private static final String CLUSTER_NAME = "cluster.name";
    private static final String CLIENT_TRANSPORT_SNIFF = "client.transport.sniff";

    @Value("${spring.data.elasticsearch.cluster-name}")
    private String elasticsearchClusterName;

    @Value("${spring.data.elasticsearch.cluster-nodes}")
    private String clusterNodes;

    private TransportClient transportClient;
    private PreBuiltTransportClient preBuiltTransportClient;


    @Override
    public TransportClient getObject() throws Exception {
        return transportClient;
    }

    @Override
    public Class<?> getObjectType() {
        return TransportClient.class;
    }

    @Override
    public boolean isSingleton() {
        return false;
    }


    @Override
    public void destroy() throws Exception {
        try {
            logger.info("Closing elasticSearch client");
            if (transportClient != null) {
                transportClient.close();
            }
        } catch (final Exception e) {
            logger.error("Error closing ElasticSearch client: ", e);
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        buildClient();

    }

    private void buildClient() {
        preBuiltTransportClient = new PreBuiltTransportClient(settings());
        String InetSocket[] = clusterNodes.split(":");
        String address = InetSocket[0];
        Integer port = Integer.valueOf(InetSocket[1]);
        try {
            transportClient = preBuiltTransportClient.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(address), port));
            /*transportClient
                    .admin()
                    .indices()
                    .prepareUpdateSettings("tweets-from-companies")
                    .setSettings(Settings
                            .builder()
                            .put("index.max_result_window", 500000))
                    .get();*/
        } catch (UnknownHostException e) {
            logger.error(e.getMessage());
        }

    }

    private Settings settings() {

        return Settings.builder()
                .put(CLUSTER_NAME, elasticsearchClusterName)
                .put(CLIENT_TRANSPORT_SNIFF, true)
                .build();
    }
}
