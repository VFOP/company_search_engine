package com.company.search.engine.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.social.twitter.api.Twitter;
import org.springframework.social.twitter.api.impl.TwitterTemplate;
import org.springframework.stereotype.Component;

/**
 * Created by flooprea on 3/29/2018
 */

@Component
public class TwitterTemplateCreator {
    private static volatile TwitterTemplate instance = null;

    @Value("${consumer-key}")
    private String consumerKey;

    @Value("${consumer-secret}")
    private String consumerSecret;

    @Value("${access-token}")
    private String accessToken;

    @Value("${access-token-secret}")
    private String accessTokenSecret;

    public Twitter getTwitterTemplate() {

        synchronized (TwitterTemplate.class) {
            if (instance == null) {
                instance = new TwitterTemplate(consumerKey, consumerSecret, accessToken, accessTokenSecret);
            }
        }
        return instance;
    }
}
