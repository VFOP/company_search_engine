package com.company.search.engine.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@ComponentScan("com.company.search.engine")
@EntityScan("com.company.search.engine.model")
@EnableElasticsearchRepositories("com.company.search.engine.repository")
@EnableAutoConfiguration()
@SpringBootApplication
public class CompanySearchEngineApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompanySearchEngineApplication.class, args);
	}
}
