package com.company.search.engine.repository;

import com.company.search.engine.model.Company;
import com.company.search.engine.model._TwitterProfile;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.social.twitter.api.TwitterProfile;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by flooprea on 4/7/2018
 */

@Repository
public interface TwitterProfileRepository extends ElasticsearchRepository<_TwitterProfile, String> {
    List<_TwitterProfile> findByCompanyId(String companyId);

    _TwitterProfile findCompanyIdByScreenName(String screenName);
}
