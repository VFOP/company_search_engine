package com.company.search.engine.repository;

import com.company.search.engine.model.Company;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by flooprea on 3/30/2018
 */

@Repository
public interface CompanyRepository extends ElasticsearchRepository<Company, String> {
    Company findByName(String name);
    List<Company> findAllByCountry(String country);
    List<Company> findAllByType(String companyType, Pageable pageable);
    void deleteByName(String name);
}
