package com.company.search.engine.repository;

import com.company.search.engine.model._Tweet;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by flooprea on 4/7/2018
 */

@Repository
public interface TweetRepository extends ElasticsearchRepository<_Tweet, String> {
    List<_Tweet> findByTwitterProfileId(String twitterProfileId);
}
