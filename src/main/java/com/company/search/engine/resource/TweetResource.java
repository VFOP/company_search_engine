package com.company.search.engine.resource;

import com.company.search.engine.config.TwitterTemplateCreator;
import com.company.search.engine.model.Event;
import com.company.search.engine.model._Tweet;
import com.company.search.engine.service.SearchQueryBuilderService;
import com.company.search.engine.service.TweetService;
import com.company.search.engine.service.TwitterProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by flooprea on 4/7/2018
 */

@RestController
@RequestMapping("/api/tweet")
public class TweetResource extends Resource {

    @Autowired
    private TweetService tweetService;

    @Autowired
    private SearchQueryBuilderService searchQueryBuilderService;

    @Autowired
    private TwitterProfileService twitterProfileService;

    @Autowired
    private TwitterTemplateCreator twitterTemplateCreator;

    @PostMapping("/tweets")
    public ResponseEntity<List<_Tweet>> indexTweets() {

        List<_Tweet> tweets = twitterProfileService.getTwitterProfiles()
                .stream()
                .flatMap(twitterProfile -> {
                    Optional<List<_Tweet>> profileTweets = tweetService.getTweetsForTwitterProfile(twitterProfile.getId());
                    return (profileTweets.isPresent()) ? profileTweets.get().stream() : null;
                })
                .collect(Collectors.toList());

        if (tweets.isEmpty()) {
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(tweetService.indexTweets(tweets), new HttpHeaders(), HttpStatus.CREATED);
    }

    @GetMapping("/tweets")
    public ResponseEntity<Page<_Tweet>> getIndexedTweets(@RequestParam(name = "page", defaultValue = "1") Integer pageNumber) {
        Pageable pageable = new PageRequest(pageNumber, 100);
        Page<_Tweet> page = tweetService.getTweets(pageable);
        return new ResponseEntity<>(page, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/tweets/{companyName}")
    public ResponseEntity<List<_Tweet>> getIndexedTweetsByCompany(@PathVariable(name = "companyName") String companyName) {
        List<_Tweet> tweets = tweetService.getTweetsByCompanyName(companyName);

        if (tweets.isEmpty()) {
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(tweets, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/events/{eventName}")
    public ResponseEntity<Page<Event>> getTweetEvents(@PathVariable(name = "eventName") String eventName,
                                                      @RequestParam(name = "page") Integer pageNumber) {
        Pageable pageable = new PageRequest(pageNumber, 100);
        Page<Event> eventPage = searchQueryBuilderService.getEvents(eventName, pageable);

        return new ResponseEntity<>(eventPage, new HttpHeaders(), HttpStatus.OK);
    }
}
