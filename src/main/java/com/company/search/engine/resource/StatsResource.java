package com.company.search.engine.resource;

import com.company.search.engine.model.CompanyType;
import com.company.search.engine.service.CompanyTypePerCountryService;
import com.company.search.engine.service.VerbsFrequencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.TreeMap;

/**
 * Created by flooprea on 6/1/2018
 */


@RestController
@RequestMapping("/api/statistic")
public class StatsResource extends Resource {

    @Autowired
    private CompanyTypePerCountryService companyTypePerCountryService;

    @Autowired
    private VerbsFrequencyService verbsFrequencyService;

    @GetMapping("/companyTypePerCountry")
    public ResponseEntity<Map<CompanyType, Map<String, Integer>>> getCompanyTypeCountry() {

        Map<CompanyType, Map<String, Integer>> companyTypesPerCountry = companyTypePerCountryService.getCompanyTypePerCountry();

        if (companyTypesPerCountry.isEmpty()) {
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.NO_CONTENT);
        }
        return new ResponseEntity<>(companyTypesPerCountry, new HttpHeaders(), HttpStatus.OK);

    }

    @GetMapping("/pronoun/{pronoun}")
    public ResponseEntity<Map<String, Map<String, Integer>>> getMostFrequentVerbs(@PathVariable(name = "pronoun") String pronoun) {

        Map<String, Map<String, Integer>> frequentVerbsMap = verbsFrequencyService.getMostFrequentVerbsAfterPronoun(pronoun);
        if (frequentVerbsMap.isEmpty()) {
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(frequentVerbsMap, new HttpHeaders(), HttpStatus.OK);
    }
}
