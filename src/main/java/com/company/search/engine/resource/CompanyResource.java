package com.company.search.engine.resource;

import com.company.search.engine.model.Company;
import com.company.search.engine.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

/**
 * Created by flooprea on 4/7/2018
 */
@RestController
@RequestMapping("/api/company")
public class CompanyResource extends Resource {
    private static final int PAGE_SIZE = 50;
    @Autowired
    private CompanyService companyService;


    @PostMapping("/testDBpedia")
    public ResponseEntity<?> testDBpedia() {
        String sparqlQuery = companyService.createSparqlQuery();
        List<Company> companies = companyService.interrogateDBPedia(sparqlQuery);

        return new ResponseEntity<>(companies, new HttpHeaders(), HttpStatus.OK);
    }

    @PostMapping("/companies")
    public ResponseEntity<List<Company>> indexCompaniesFromDBPedia() {


        String sparqlQuery = companyService.createSparqlQuery();
        List<Company> companies = companyService.interrogateDBPedia(sparqlQuery);

        List<Company> indexedCompanies = companyService.indexCompanies(companies);

        if (indexedCompanies.isEmpty()) {
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(companies, new HttpHeaders(), HttpStatus.CREATED);
    }

    @GetMapping("/companies")
    public ResponseEntity<Page<Company>> getIndexedCompanies(@RequestParam(value = "page", defaultValue = "1") Integer pageNumber) {
        Pageable pageable = new PageRequest(pageNumber, PAGE_SIZE, new Sort(Sort.Direction.DESC, "employeeNumber"));
        Page<Company> page = companyService.getCompanies(pageable);
        return new ResponseEntity<>(page, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/companiesNumber")
    public ResponseEntity<Long> getNumberOfPages() {
        Long numberOfPages = companyService.getNumberOfCompanies() / PAGE_SIZE;
        return new ResponseEntity<>(numberOfPages, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping(value = "/company/{id}")
    public ResponseEntity<Company> getCompanyById(@PathVariable(name = "id") String id) {
        Optional<Company> company = companyService.getCompanyById(id);
        return company.map(c -> new ResponseEntity<>(c, new HttpHeaders(), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(new HttpHeaders(), HttpStatus.NO_CONTENT));
    }

    @DeleteMapping("/companies")
    public ResponseEntity<?> deleteCompanies() {
        companyService.deleteCompanies();

        return new ResponseEntity<>(new HttpHeaders(), HttpStatus.NO_CONTENT);
    }

}
