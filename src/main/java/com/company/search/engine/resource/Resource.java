package com.company.search.engine.resource;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by flooprea on 3/29/2018
 */

@RequestMapping("/")
public class Resource {
    @GetMapping
    public ResponseEntity<?> hello() {
        return new ResponseEntity<>("Hello World!", new HttpHeaders(), HttpStatus.OK);
    }
}
