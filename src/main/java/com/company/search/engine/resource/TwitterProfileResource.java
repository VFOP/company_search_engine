package com.company.search.engine.resource;

import com.company.search.engine.model._TwitterProfile;
import com.company.search.engine.service.CompanyService;
import com.company.search.engine.service.ExcelService;
import com.company.search.engine.service.TwitterProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by flooprea on 4/7/2018
 */

@RestController
@RequestMapping("/api/twitterProfile")
public class TwitterProfileResource extends Resource {

    public static final int PAGE_SIZE = 50;
    @Autowired
    private TwitterProfileService twitterProfileService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private ExcelService excelService;

    @PostMapping("/twitterProfiles")
    public ResponseEntity<List<_TwitterProfile>> indexTwitterProfiles() {

        List<_TwitterProfile> twitterProfiles = companyService.getCompanies()
                .stream()
                .flatMap(company -> {
                    Optional<List<_TwitterProfile>> profiles = twitterProfileService.getTwitterProfilesForCompany(company.getName());
                    return (profiles.isPresent()) ? profiles.get().stream() : null;
                })
                .collect(Collectors.toList());

        if (twitterProfiles.isEmpty()) {
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.NO_CONTENT);
        }

        return new ResponseEntity<>(twitterProfileService.indexTwitterProfiles(twitterProfiles), new HttpHeaders(), HttpStatus.CREATED);
    }

    @GetMapping("/twitterProfiles")
    public ResponseEntity<Page<_TwitterProfile>> getIndexedTwitterProfiles(@RequestParam(value = "page", defaultValue = "1") Integer pageNumber) {
        Pageable pageable = new PageRequest(pageNumber, PAGE_SIZE);

        Page<_TwitterProfile> page = twitterProfileService.getTwitterProfiles(pageable);

        return new ResponseEntity<>(page, new HttpHeaders(), HttpStatus.OK);
    }

    @GetMapping("/randomProfiles/{totalNumber}")
    public ResponseEntity<List<String>> getRandomTwitterProfiles(@PathVariable(name = "totalNumber") Integer totalNumber) {

        List<String> randomProfileScreenNames = twitterProfileService.getRandomTwitterProfiles(totalNumber);

        if (randomProfileScreenNames.isEmpty()) {
            return new ResponseEntity<>(new HttpHeaders(), HttpStatus.NO_CONTENT);
        }

        excelService.createExcel(randomProfileScreenNames);

        twitterProfileService.openChrome(randomProfileScreenNames.subList(0,19));
        twitterProfileService.openChrome(randomProfileScreenNames.subList(20,39));
        twitterProfileService.openChrome(randomProfileScreenNames.subList(40,59));
        twitterProfileService.openChrome(randomProfileScreenNames.subList(60,79));
        twitterProfileService.openChrome(randomProfileScreenNames.subList(80,99));
        twitterProfileService.openChrome(randomProfileScreenNames.subList(100,119));
        twitterProfileService.openChrome(randomProfileScreenNames.subList(120,139));
        twitterProfileService.openChrome(randomProfileScreenNames.subList(140,159));
        twitterProfileService.openChrome(randomProfileScreenNames.subList(160,179));
        twitterProfileService.openChrome(randomProfileScreenNames.subList(180,199));

        return new ResponseEntity<>(randomProfileScreenNames, new HttpHeaders(), HttpStatus.OK);
    }
}
