package com.company.search.engine.service.impl;

import com.company.search.engine.service.CoreNLPService;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

/**
 * Created by flooprea on 9/9/2018
 */

@Service
public class CoreNLPServiceImpl implements CoreNLPService {
    public static final String UNDERSCORE = "_";
    public static final String ANNOTATORS = "annotators";
    public static final String TOKENIZE_SSPLIT_POS = "tokenize, ssplit, pos";
    public static final String TOKENIZE_SSPLIT_POS_LEMA = "tokenize, ssplit, pos, lemma";


    public List<String> getTaggedWordsStanfordCoreNLP(String text) {
        List<String> taggedWordList = new ArrayList<>();
        Annotation annotation = getAnnotation(text, TOKENIZE_SSPLIT_POS);

        List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);
        for (CoreMap sentence : sentences) {
            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                String word = token.get(CoreAnnotations.TextAnnotation.class);
                String pos = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
                taggedWordList.add(word + UNDERSCORE + pos);
            }
        }
        return taggedWordList;
    }

    public List<String> lemmatize(String verbalConstruction) {
        List<String> lemmas = new LinkedList<>();

        Annotation annotation = getAnnotation(verbalConstruction, TOKENIZE_SSPLIT_POS_LEMA);

        List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);
        for (CoreMap sentence : sentences) {
            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                String term = token.get(CoreAnnotations.LemmaAnnotation.class);
                if (!term.equals("be") && !term.equals("have")) {
                    lemmas.add(term);
                }
            }
        }
        return lemmas;
    }

    private Annotation getAnnotation(String text, String tokenizeSsplitPos) {
        Properties properties = new Properties();
        properties.setProperty(ANNOTATORS, tokenizeSsplitPos);
        StanfordCoreNLP pipeline = new StanfordCoreNLP(properties);
        Annotation annotation = new Annotation(text);
        pipeline.annotate(annotation);
        return annotation;
    }
}
