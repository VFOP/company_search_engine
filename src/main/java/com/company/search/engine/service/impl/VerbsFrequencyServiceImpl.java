package com.company.search.engine.service.impl;

import com.company.search.engine.model._Tweet;
import com.company.search.engine.service.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

/**
 * Created by flooprea on 8/20/2018
 */

@Service
public class VerbsFrequencyServiceImpl implements VerbsFrequencyService {

    public static final String UNDERSCORE = "_";
    public static final String SPACE = " ";

    @Autowired
    private SearchQueryBuilderService searchQueryBuilderService;

    @Autowired
    private TwitterProfileService twitterProfileService;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private CoreNLPService coreNLPService;

    private static final List<String> VALID_POS = new ArrayList<>(Arrays.asList("VB", "VBD", "VBG", "VBN", "VBP", "VBZ"/*, "TO", "MD", "RB", "RBR", "RBS"*/));

    @Override
    public Map<String, Map<String, Integer>> getMostFrequentVerbsAfterPronoun(String pronoun) {
        Map<String, Integer> companyVerbsFrequencyMap = new LinkedHashMap<>();
        Map<String, Map<String, Integer>> verbsFrequencyMap = new LinkedHashMap<>();
        List<_Tweet> tweets = searchQueryBuilderService.findTweetsByTerm(pronoun);

        for (int i = 0; i < tweets.size(); i++) {
            List<String> taggedWordList = coreNLPService.getTaggedWordsStanfordCoreNLP(tweets.get(i).getText());
            String fromUser = tweets.get(i).getFromUser();
            String companyId = twitterProfileService.getCompanyIdByScreenName(fromUser);
            String companyName = companyService.getCompanyNameById(companyId);

            if (verbsFrequencyMap.containsKey(companyName)) {
                companyVerbsFrequencyMap = verbsFrequencyMap.get(companyName);
                computeFrequentVerbsMap(taggedWordList, pronoun, companyVerbsFrequencyMap);
            } else {
                companyVerbsFrequencyMap = new LinkedHashMap<>();
                computeFrequentVerbsMap(taggedWordList, pronoun, companyVerbsFrequencyMap);
                verbsFrequencyMap.put(companyName, companyVerbsFrequencyMap);
            }
        }

        return lemmatizeVerbalConstructions(verbsFrequencyMap);
    }

    protected void computeFrequentVerbsMap(List<String> taggedWordList, String pronoun, Map<String, Integer> verbsFrequencyMap) {
        int i = 0;
        while (i < taggedWordList.size() - 2) {
            String[] currentWordTag = taggedWordList.get(i).split(UNDERSCORE);
            if (currentWordTag[0].equalsIgnoreCase(pronoun)) {
                String[] nextWordTag = taggedWordList.get(i + 1).split(UNDERSCORE);
                if (VALID_POS.contains(nextWordTag[1])) {
                    StringBuilder builder = new StringBuilder(nextWordTag[0]);
                    i++;
                    nextWordTag = taggedWordList.get(i + 1).split(UNDERSCORE);

                    while (VALID_POS.contains(nextWordTag[1]) && i < taggedWordList.size() - 2) {
                        builder.append(SPACE);
                        builder.append(nextWordTag[0]);
                        i++;
                        nextWordTag = taggedWordList.get(i + 1).split(UNDERSCORE);
                    }
                    String verb = builder.toString().toLowerCase();
                    if (verbsFrequencyMap.containsKey(verb)) {
                        int occurrences = verbsFrequencyMap.get(verb);
                        verbsFrequencyMap.put(verb, occurrences + 1);
                    } else {
                        verbsFrequencyMap.put(verb, 1);
                    }
                }
            }
            i++;
        }
    }

    private Map<String, Map<String, Integer>> lemmatizeVerbalConstructions(Map<String, Map<String, Integer>> verbsFrequencyMap) {
        Map<String, Map<String, Integer>> lemmatizedVerbsFrequencyMap = new LinkedHashMap<>();

        verbsFrequencyMap.forEach((company, companyVerbsFrequencyMap) -> {
            Map<String, Integer> lemmatizedCompanyVerbsFrequencyMap = new LinkedHashMap<>();

            companyVerbsFrequencyMap.forEach((verbalConstruction, frequency) -> {
                List<String> lemmatizedVerbalConstruction = coreNLPService.lemmatize(verbalConstruction);

                lemmatizedVerbalConstruction.forEach(verb -> {
                    if (lemmatizedCompanyVerbsFrequencyMap.containsKey(verb)) {
                        Integer existingFrequency = lemmatizedCompanyVerbsFrequencyMap.get(verb);
                        lemmatizedCompanyVerbsFrequencyMap.put(verb, existingFrequency + frequency);
                    } else {
                        lemmatizedCompanyVerbsFrequencyMap.put(verb, frequency);
                    }
                });
            });
            lemmatizedVerbsFrequencyMap.put(company, lemmatizedCompanyVerbsFrequencyMap);
        });
        return lemmatizedVerbsFrequencyMap;
    }
}
