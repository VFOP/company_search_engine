package com.company.search.engine.service.impl;

import com.company.search.engine.config.TwitterTemplateCreator;
import com.company.search.engine.model._TwitterProfile;
import com.company.search.engine.repository.CompanyRepository;
import com.company.search.engine.repository.TwitterProfileRepository;
import com.company.search.engine.service.TwitterProfileService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.social.twitter.api.TwitterProfile;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by flooprea on 3/31/2018
 */

@Service
public class TwitterProfileServiceImpl implements TwitterProfileService {

    @Autowired
    private TwitterProfileRepository twitterProfileRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private TwitterTemplateCreator twitterTemplateCreator;

    @Override
    public Optional<List<_TwitterProfile>> getTwitterProfilesForCompany(String companyName) {
        List<_TwitterProfile> twitterProfiles = null;
        try {
            twitterProfiles = getAssociatedProfiles(companyName)
                    .stream()
                    .map(twitterProfile -> {
                        _TwitterProfile profile = new _TwitterProfile();
                        profile.setId(String.valueOf(twitterProfile.getId()));
                        profile.setScreenName(twitterProfile.getScreenName());
                        profile.setName(twitterProfile.getName());
                        profile.setCompanyId(companyRepository.findByName(companyName).getId());
                        profile.setVerified(twitterProfile.isVerified());
                        profile.setDescription(twitterProfile.getDescription());

                        return profile;

                    }).collect(Collectors.toList());
        } catch (Exception e) {
            LoggerFactory.getLogger(TwitterProfileServiceImpl.class).error(e.getMessage());
        }

        return Optional.ofNullable(twitterProfiles);
    }

    @Override
    public _TwitterProfile indexTwitterProfile(_TwitterProfile twitterProfile) {
        return twitterProfileRepository.index(twitterProfile);
    }

    @Override
    public List<_TwitterProfile> indexTwitterProfiles(List<_TwitterProfile> twitterProfiles) {
        List<_TwitterProfile> indexedTwitterProfiles = new ArrayList<>();
        twitterProfiles.forEach(twitterProfile -> indexedTwitterProfiles.add(indexTwitterProfile(twitterProfile)));

        return twitterProfiles;
    }

    @Override
    public Page<_TwitterProfile> getTwitterProfiles(Pageable pageable) {
        return twitterProfileRepository.findAll(pageable);
    }

    @Override
    public List<_TwitterProfile> getTwitterProfiles() {
        List<_TwitterProfile> twitterProfiles = new ArrayList<>();
        twitterProfileRepository.findAll().forEach(twitterProfiles::add);

        return twitterProfiles;
    }

    @Override
    public List<_TwitterProfile> getTwitterProfilesByCompanyId(String companyId) {
        return twitterProfileRepository.findByCompanyId(companyId);
    }

    @Override
    public List<_TwitterProfile> getTwitterProfilesByCompanyName(String companyName) {
        return getTwitterProfilesByCompanyId(companyRepository.findByName(companyName).getId());
    }

    @Override
    public Optional<_TwitterProfile> getTwitterProfileById(String id) {
        return twitterProfileRepository.findById(id);
    }

    @Override
    public String getCompanyIdByScreenName(String screenName) {
        return twitterProfileRepository.findCompanyIdByScreenName(screenName).getCompanyId();
    }

    @Override
    public void deleteTwitterProfiles() {
        twitterProfileRepository.deleteAll();
    }

    @Override
    public void deleteTwitterProfileById(String id) {
        twitterProfileRepository.deleteById(id);
    }

    @Override
    public void deleteTwitterProfilesByCompanyId(String companyId) {
        getTwitterProfilesByCompanyId(companyId).forEach(twitterProfile -> deleteTwitterProfileById(twitterProfile.getId()));
    }

    @Override
    public void deleteTwitterProfilesByCompanyName(String companyName) {
        getTwitterProfilesByCompanyName(companyName).forEach(twitterProfile -> deleteTwitterProfileById(twitterProfile.getId()));

    }

    private List<TwitterProfile> getAssociatedProfiles(String companyName) {
        return twitterTemplateCreator
                .getTwitterTemplate()
                .userOperations()
                .searchForUsers(companyName)
                .stream()
                .filter(twitterProfile -> match(twitterProfile, companyName))
                .collect(Collectors.toList());
    }

    private boolean match(TwitterProfile twitterProfile, String companyName) {
        return twitterProfile.getName().contains(companyName) ||
                companyName.contains(twitterProfile.getName()) ||
                twitterProfile.getScreenName().startsWith(companyName) ||
                companyName.startsWith(twitterProfile.getScreenName());
    }

    public List<String> getRandomTwitterProfiles(Integer totalNumber) {
        List<_TwitterProfile> twitterProfiles = getTwitterProfiles();
        List<String> randomProfiles = new ArrayList<>();
        for (int i = 0; i < totalNumber; i++) {
            String profile = twitterProfiles.get(new Random().nextInt(twitterProfiles.size())).getScreenName();
            randomProfiles.add(profile);
        }

        return randomProfiles;
    }

    public void openChrome(List<String> randomTwitterProfiles) {
        randomTwitterProfiles.forEach(profile -> {
            Process p = null;
            try {
                p = Runtime.getRuntime().exec(new String[]{"\"/Program Files (x86)/Google/Chrome/Application/chrome.exe\"", "https://twitter.com/" + profile});
                p.waitFor();
            } catch (Exception e) {
                Logger.getLogger(TwitterProfileServiceImpl.class.getName()).log(Level.SEVERE, e.getMessage());
            }

        });
    }
}
