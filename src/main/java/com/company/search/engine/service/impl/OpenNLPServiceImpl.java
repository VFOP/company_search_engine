package com.company.search.engine.service.impl;

import com.company.search.engine.service.OpenNLPService;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by flooprea on 9/10/2018
 */

@Service
public class OpenNLPServiceImpl implements OpenNLPService {
    private static final String UNDERSCORE = "_";
    private static final String SPACE = " ";
    private static final String PATH_NAME = "src/main/resources/en-pos-maxent.bin";

    public List<String> getTaggedWordsOpenNLP(String text) {
        String[] wordList = text.split(SPACE);
        List<String> taggedWordList = new ArrayList<>();

        try (InputStream modelIn = new FileInputStream(new File(PATH_NAME))) {
            POSModel posModel = new POSModel(modelIn);
            POSTaggerME posTaggerME = new POSTaggerME(posModel);

            String tags[] = posTaggerME.tag(wordList);
            for (int i = 0; i < tags.length; i++) {
                taggedWordList.add(wordList[i] + UNDERSCORE + tags[i]);
            }
        } catch (IOException e) {
            Logger.getLogger(OpenNLPServiceImpl.class.getName()).log(Level.SEVERE, e.getMessage());
        }
        return taggedWordList;
    }
}
