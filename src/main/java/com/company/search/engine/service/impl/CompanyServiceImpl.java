package com.company.search.engine.service.impl;

import com.company.search.engine.model.Company;
import com.company.search.engine.model.EventType;
import com.company.search.engine.model._Tweet;
import com.company.search.engine.repository.CompanyRepository;
import com.company.search.engine.service.CompanyService;
import com.company.search.engine.service.SearchQueryBuilderService;
import org.apache.jena.query.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * Created by flooprea on 3/29/2018
 */

@Service
public class CompanyServiceImpl implements CompanyService {
    private static final String HTTP_DBPEDIA_ORG_SPARQL = "http://dbpedia.org/sparql";

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private SearchQueryBuilderService searchQueryBuilderService;

    @Autowired
    ElasticsearchTemplate elasticsearchTemplate;

    public String createSparqlQuery() {
        StringBuilder builder = new StringBuilder();

        builder.append("PREFIX o: <http://dbpedia.org/ontology/>").append("\n");
        builder.append("PREFIX p: <http://dbpedia.org/property/>").append("\n");
        builder.append("PREFIX r: <http://dbpedia.org/resource/>").append("\n");
        builder.append("PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>").append("\n");
        builder.append("PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>").append("\n");
        builder.append("SELECT *").append("\n");
        builder.append("WHERE {").append("\n");
        builder.append("?uri a o:Company;").append("\n");
        builder.append("o:numberOfEmployees ?numberOfEmployees;").append("\n");
        builder.append("rdfs:label ?label;").append("\n");
        builder.append("o:locationCountry ?locationCountry;").append("\n");
        builder.append("o:type ?type.").append("\n");
        builder.append("FILTER langMatches(lang(?label),'en')").append("\n");
        builder.append("FILTER (?type IN (r:Public_company, r:Public_Company, " +
                                         "r:Private_Company, r:Private_company, " +
                                         "r:Public_limited_company, r:Limited_Company, " +
                                         "r:Privately_held_company, r:Limited_company, " +
                                         "r:Subsidiary)) ").append("\n");
        builder.append("}").append("\n");
        builder.append("ORDER BY DESC (?numberOfEmployees)");
        //builder.append("LIMIT 1000");

        return builder.toString();
    }

    @Override
    public List<Company> interrogateDBPedia(String sparqlQuery) {
        Query query = QueryFactory.create(sparqlQuery);

        List<Company> companies = new ArrayList<>();

        try (QueryExecution qexec = QueryExecutionFactory.sparqlService(HTTP_DBPEDIA_ORG_SPARQL, query)) {
            ResultSet resultSet = qexec.execSelect();
            while ((resultSet.hasNext())) {

                QuerySolution data = resultSet.nextSolution();
                Company company = new Company();
                company.setId(String.valueOf(resultSet.getRowNumber()));
                company.setEmployeeNumber(data.get("?numberOfEmployees").asLiteral().getInt());
                company.setName(data.getLiteral("?label").getString());
                company.setUri(data.getResource("?uri").toString());
                company.setCountry(data.getResource("?locationCountry")
                        .getURI().substring(data.getResource("?locationCountry").getURI().lastIndexOf("/") + 1)
                        .replace("-", " ").replace("_", " "));
                company.setType(data.getResource("?type").getLocalName().replace("-", " ").replace("_", " ").toUpperCase());

                companies.add(company);
            }
        }
        return companies;
    }

    @Override
    public Company indexCompany(Company company) {
        return companyRepository.index(company);
    }

    @Override
    public Page<Company> getCompanies(Pageable pageable) {
        Page<Company> companyPage = companyRepository.findAll(pageable);
        companyPage.getContent().forEach(company -> {
            Map<EventType, List<_Tweet>> event = new HashMap<>();
            Arrays.stream(EventType.values())
                    .forEach(eventType -> {
                        Optional<List<_Tweet>> tweets = searchQueryBuilderService
                                .findTweetsByCompanyNameAndEvent(company.getName(), eventType.getEventName());
                        tweets.ifPresent(tweetList -> event.put(eventType, tweetList));
                    });
            company.setEvents(event);
        });

        return companyPage;
    }

    public List<Company> getCompanies() {
        List<Company> companies = new ArrayList<>();
        companyRepository.findAll().forEach(companies::add);

        return companies;
    }

    @Override
    public Long getNumberOfCompanies() {
        return companyRepository.count();
    }

    @Override
    public List<Company> indexCompanies(List<Company> companies) {
        List<Company> indexedCompanies = new ArrayList<>();
        companies.forEach(company -> indexedCompanies.add(indexCompany(company)));

        return indexedCompanies;
    }

    @Override
    public Optional<Company> getCompanyById(String id) {
        return companyRepository.findById(id);
    }

    public Company getCompanyByName(String name) {
        return companyRepository.findByName(name);
    }

    @Override
    public List<Company> getCompaniesByCountry(String country) {
        return companyRepository.findAllByCountry(country);
    }

    @Override
    public Company updateCompany(String name) {
        return null; //TODO
    }

    @Override
    public void deleteCompanies() {
        companyRepository.deleteAll();
    }

    @Override
    public void deleteCompanyById(String id) {
        companyRepository.deleteById(id);
    }

    @Override
    public void deleteCompanyByName(String name) {
        companyRepository.deleteByName(name);
    }

    @Override
    public String getCompanyNameById(String companyId) {
        return companyRepository.findById(companyId).get().getName();
    }
}
