package com.company.search.engine.service;

import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by flooprea on 9/10/2018
 */

@Service
public interface ExcelService {

    public void createExcel(List<String> twitterProfileUrls);
}
