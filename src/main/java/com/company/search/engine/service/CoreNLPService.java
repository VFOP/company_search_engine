package com.company.search.engine.service;

import edu.stanford.nlp.pipeline.Annotation;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by flooprea on 9/9/2018
 */

@Service
public interface CoreNLPService {

    List<String> getTaggedWordsStanfordCoreNLP(String text);
    List<String> lemmatize(String verbalConstruction);
}
