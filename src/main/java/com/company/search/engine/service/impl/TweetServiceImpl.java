package com.company.search.engine.service.impl;

import com.company.search.engine.config.TwitterTemplateCreator;
import com.company.search.engine.model._Tweet;
import com.company.search.engine.model._TwitterProfile;
import com.company.search.engine.repository.CompanyRepository;
import com.company.search.engine.repository.TweetRepository;
import com.company.search.engine.repository.TwitterProfileRepository;
import com.company.search.engine.service.TweetService;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.social.twitter.api.Tweet;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by flooprea on 4/7/2018
 */

@Service
public class TweetServiceImpl implements TweetService {

    private static final int PAGE_SIZE = 100;
    @Autowired
    private TweetRepository tweetRepository;

    @Autowired
    private TwitterTemplateCreator twitterTemplateCreator;

    @Autowired
    private TwitterProfileRepository twitterProfileRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public Optional<List<_Tweet>> getTweetsForTwitterProfile(String twitterProfileId) {
        List<_Tweet> tweets = null;
        Optional<_TwitterProfile> profile = twitterProfileRepository.findById(twitterProfileId);
        if (profile.isPresent()) {
            Optional<List<Tweet>> tweetsStream = getTimelineTweets(profile.get());
            if (tweetsStream.isPresent()) {
                tweets = tweetsStream
                        .get()
                        .stream()
                        .map(t -> {
                            _Tweet tweet = new _Tweet();
                            tweet.setId(String.valueOf(t.getId()));
                            tweet.setText(t.getText());
                            tweet.setCreatedAt(t.getCreatedAt());
                            tweet.setFromUser(t.getFromUser());
                            tweet.setTwitterProfileId(twitterProfileId);

                            return tweet;
                        }).collect(Collectors.toList());
            }
        }

        return Optional.ofNullable(tweets);
    }

    @Override
    public _Tweet indexTweet(_Tweet tweet) {
        return tweetRepository.index(tweet);
    }

    @Override
    public List<_Tweet> indexTweets(List<_Tweet> tweets) {
        List<_Tweet> indexedTweets = new ArrayList<>();
        tweets.forEach(tweet -> indexedTweets.add(indexTweet(tweet)));

        return indexedTweets;
    }

    @Override
    public Page<_Tweet> getTweets(Pageable pageable) {
        return tweetRepository.findAll(pageable);
    }

    @Override
    public List<_Tweet> getTweetsByTwitterProfileId(String twitterProfileId) {
        return tweetRepository.findByTwitterProfileId(twitterProfileId);
    }

    @Override
    public List<_Tweet> getTweetsByCompanyName(String companyName) {
        return twitterProfileRepository
                .findByCompanyId(companyRepository.findByName(companyName).getId())
                .stream()
                .flatMap(twitterProfile -> {
                    Optional<List<Tweet>> tweetsStream = getTimelineTweets(twitterProfile);
                    if (tweetsStream.isPresent()) {
                        return tweetsStream.get().stream()
                                .map(t -> {
                                    t.getExtraData();
                                    _Tweet tweet = new _Tweet();
                                    tweet.setId(String.valueOf(t.getId()));
                                    tweet.setText(t.getText());
                                    tweet.setCreatedAt(t.getCreatedAt());
                                    tweet.setFromUser(t.getFromUser());
                                    tweet.setTwitterProfileId(twitterProfile.getId());

                                    return tweet;
                                })
                                .collect(Collectors.toList())
                                .stream();

                    }
                    return null;

                })
                .collect(Collectors.toList());
    }

    private Optional<List<Tweet>> getTimelineTweets(_TwitterProfile twitterProfile) {
        List<Tweet> tweets = null;
        try {
            tweets = twitterTemplateCreator
                    .getTwitterTemplate()
                    .timelineOperations()
                    .getUserTimeline(twitterProfile.getScreenName(), PAGE_SIZE);
        } catch (Exception e) {
            LoggerFactory.getLogger(TweetServiceImpl.class).error(e.getMessage());
        }
        return Optional.ofNullable(tweets);
    }
}
