package com.company.search.engine.service;

import com.company.search.engine.model.Event;
import com.company.search.engine.model._Tweet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by flooprea on 5/5/2018
 */

@Service
public interface SearchQueryBuilderService {
    Page<Event> getEvents(String event, Pageable pageable);
    Optional<List<_Tweet>> findTweetsByCompanyNameAndEvent(String companyName, String event);
    List<_Tweet> findTweetsByTerm(String term);
}
