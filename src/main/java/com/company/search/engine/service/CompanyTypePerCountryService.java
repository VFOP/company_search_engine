package com.company.search.engine.service;

import com.company.search.engine.model.CompanyType;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by flooprea on 6/1/2018
 */

@Service
public interface CompanyTypePerCountryService {

    Map<CompanyType, Map<String, Integer>> getCompanyTypePerCountry();

}
