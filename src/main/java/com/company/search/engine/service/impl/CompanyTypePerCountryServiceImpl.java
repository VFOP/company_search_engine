package com.company.search.engine.service.impl;

import com.company.search.engine.model.CompanyType;
import com.company.search.engine.repository.CompanyRepository;
import com.company.search.engine.service.CompanyTypePerCountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by flooprea on 6/1/2018
 */

@Service
public class CompanyTypePerCountryServiceImpl implements CompanyTypePerCountryService {

    @Autowired
    private CompanyRepository companyRepository;


    @Override
    public Map<CompanyType, Map<String, Integer>> getCompanyTypePerCountry() {
        Map<CompanyType, Map<String, Integer>> companyTypePerCountry = new TreeMap<>();
        Arrays.stream(CompanyType.values()).forEach(companyType -> {
            Map<String, Integer> publicCompanyCountry = new TreeMap<>();
            companyRepository.findAll()
                    .iterator()
                    .forEachRemaining(company -> {
                        if (company.getType().toUpperCase().equals(companyType.getCompanyType().toUpperCase())) {
                            String k = company.getCountry();
                            publicCompanyCountry.merge(k, 1, (a, b) -> a + b);
                        }
                    });
            companyTypePerCountry.put(companyType, publicCompanyCountry);
        });
        return processCompanyTypePerCountryMap(companyTypePerCountry);
    }


    private Map<CompanyType, Map<String, Integer>> processCompanyTypePerCountryMap(Map<CompanyType, Map<String, Integer>> resultMap) {
        Set<String> countries = getCountries(resultMap);
        levelAllCompanyTypes(resultMap, countries);
        standardizeCountryNames(resultMap);

        removeCountryWithoutCompanies(resultMap, countries);

        return resultMap;

    }

    private Set<String> getCountries(Map<CompanyType, Map<String, Integer>> resultMap) {
        return resultMap
                .values()
                .stream()
                .flatMap(m-> m.keySet()
                        .stream())
                .collect(Collectors.toSet());
    }

    private void levelAllCompanyTypes(Map<CompanyType, Map<String, Integer>> resultMap, Set<String> countries) {
        resultMap.values()
                .forEach(m-> {
                    countries.forEach(country-> {
                        m.putIfAbsent(country, 0);
                    });
                });
    }

    private void standardizeCountryNames(Map<CompanyType, Map<String, Integer>> resultMap) {
        resultMap.entrySet()
                .stream()
                .map(Map.Entry::getValue)
                .forEach(m -> m.forEach((k, v) -> {
                    parseName(m, k, "UK", "United Kingdom");
                    parseName(m, k, "UK", "England");
                    parseName(m, k, "UK", "London");
                    parseName(m, k, "United states", "United States of America");
                    parseName(m, k, "United states", "Washington (state)");
                    parseName(m, k, "United states", "Waltham, Massachusetts");
                    parseName(m, k, "United states", "Ohio");
                    parseName(m, k, "United states", "Wilmington, Ohio");
                    parseName(m, k, "United states", "California");
                    parseName(m, k, "United States", "United states");
                    parseName(m, k, "Japan", "Shizuoka City");
                    parseName(m, k, "Switzerland", "Zürich, Switzerland");
                    parseName(m, k, "Russia", "Russian Federation");
                    parseName(m, k, "China", "People’s Republic of China");
                    parseName(m, k, "China", "The People's Republic of China");
                    parseName(m, k, "China", "The People’s Republic of China");
                    parseName(m, k, "China", "People's Republic of China");
                    parseName(m, k, "China", "Republic of China");
                    parseName(m, k, "Canada", "Alberta, Canada");
                    parseName(m, k, "Ireland", "Dublin");
                    parseName(m, k, "Germany", "Cologne");
                    parseName(m, k, "Netherlands", "The Netherlands");
                }));
    }

    private void parseName(Map<String, Integer> m, String k, String value, String toReplace) {
        if (k.toUpperCase().equals(value.toUpperCase()) && m.get(toReplace) != null) {
            Integer val = m.get(toReplace);
            m.put(value, m.get(value) + val);
            m.put(toReplace, 0);
        }
    }

    private void removeCountryWithoutCompanies(Map<CompanyType, Map<String, Integer>> resultMap, Set<String> countries) {
        countries.forEach(country -> {
            Long x = resultMap.values().stream().filter(map-> map.get(country) == 0).count();
            if (x == resultMap.keySet().size()) {
                resultMap.values().forEach(m-> {
                    m.remove(country);
                });
            }
        });
    }
}
