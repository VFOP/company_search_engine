package com.company.search.engine.service;

import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by flooprea on 8/20/2018
 */

@Service
public interface VerbsFrequencyService {

    Map<String, Map<String, Integer>> getMostFrequentVerbsAfterPronoun(String pronoun);
}
