package com.company.search.engine.service;

import com.company.search.engine.model._Tweet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.social.twitter.api.TwitterProfile;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by flooprea on 4/7/2018
 */

@Service
public interface TweetService {
    Optional<List<_Tweet>> getTweetsForTwitterProfile(String twitterProfileId);

    _Tweet indexTweet(_Tweet tweet);

    List<_Tweet> indexTweets(List<_Tweet> tweets);

    Page<_Tweet> getTweets(Pageable pageable);

    List<_Tweet> getTweetsByTwitterProfileId(String twitterProfileId);

    List<_Tweet> getTweetsByCompanyName(String companyName);


}
