package com.company.search.engine.service;

import com.company.search.engine.model.Company;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by flooprea on 3/29/2018
 */

@Service
public interface CompanyService {
    String createSparqlQuery();

    List<Company> interrogateDBPedia(String sparqlQuery);

    List<Company> indexCompanies(List<Company> companies);

    Company indexCompany(Company company);

    Page<Company> getCompanies(Pageable pageable);

    List<Company> getCompanies();

    Long getNumberOfCompanies();

    Optional<Company> getCompanyById(String id);

    Company getCompanyByName(String name);

    List<Company> getCompaniesByCountry(String country);

    Company updateCompany(String name);

    void deleteCompanies();

    void deleteCompanyById(String id);

    void deleteCompanyByName(String name);

    String getCompanyNameById(String companyId);
}
