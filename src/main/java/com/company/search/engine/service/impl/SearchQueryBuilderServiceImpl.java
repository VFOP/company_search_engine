package com.company.search.engine.service.impl;

import com.company.search.engine.model.Event;
import com.company.search.engine.model._Tweet;
import com.company.search.engine.model._TwitterProfile;
import com.company.search.engine.service.CompanyService;
import com.company.search.engine.service.SearchQueryBuilderService;
import com.company.search.engine.service.TwitterProfileService;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by flooprea on 5/5/2018
 */

@Service
public class SearchQueryBuilderServiceImpl implements SearchQueryBuilderService {

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    private TwitterProfileService twitterProfileService;

    @Autowired
    private CompanyService companyService;

    @Override
    public Page<Event> getEvents(String event, Pageable pageable) {
        List<_Tweet> tweetList = getTweetsByTerm(event);
        List<_Tweet> tweetSubList = paginateList(tweetList, pageable.getPageNumber(), pageable.getPageSize());
        List<Event> events = getEventsFromTweets(tweetSubList);

        return new PageImpl<>(events, pageable, tweetList.size());
    }

    @Override
    public Optional<List<_Tweet>> findTweetsByCompanyNameAndEvent(String companyName, String event) {
        List<_Tweet> tweetList = getTweetsByTerm(event);

        return Optional.ofNullable(tweetList.stream().filter(tweet -> {
            _TwitterProfile twitterProfile = twitterProfileService.getTwitterProfileById(tweet.getTwitterProfileId()).orElse(null);
            String name = companyService.getCompanyById(twitterProfile.getCompanyId()).orElse(null).getName();
            return companyName.equals(name);
        }).collect(Collectors.toList()));
    }

    @Override
    public List<_Tweet> findTweetsByTerm(String term) {
        return getTweetsByTerm(term);
    }

    private List<_Tweet> getTweetsByTerm(String eventName) {
        QueryBuilder queryBuilder = QueryBuilders.matchPhraseQuery("text", eventName);
        NativeSearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(queryBuilder).build();
        List<_Tweet> tweetList = new ArrayList<>();
        elasticsearchTemplate.stream(searchQuery, _Tweet.class).forEachRemaining(tweetList::add);
        return tweetList;
    }

    private List<_Tweet> paginateList(List<_Tweet> tweets, Integer pageNumber, Integer pageSize) {
        int from = (pageNumber - 1) * pageSize;
        int to = tweets.size() < pageNumber * pageSize ? tweets.size() : pageNumber * pageSize;

        if (pageNumber == 1) {
            return tweets.subList(from, to);
        }

        return tweets.subList(from, to);
    }

    //TODO 0->6 7-12

    private List<Event> getEventsFromTweets(List<_Tweet> tweetList) {
        return tweetList.
                stream()
                .map(tweet -> {
                    Event event1 = new Event();
                    twitterProfileService.getTwitterProfileById(tweet.getTwitterProfileId());
                    event1.setText(tweet.getText());
                    String createBy = twitterProfileService.getTwitterProfileById(tweet.getTwitterProfileId()).orElse(null).getName();
                    event1.setCreatedBy(Objects.requireNonNull(createBy != null ? createBy : tweet.getFromUser()));
                    event1.setCreateAt(tweet.getCreatedAt());
                    return event1;
                })
                .collect(Collectors.toList());
    }
}
