package com.company.search.engine.service.impl;

import com.company.search.engine.service.ExcelService;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by flooprea on 9/10/2018
 */

@Service
public class ExcelServiceImpl implements ExcelService {

    private static final String FILE_NAME = "TWITTER_PROFILE_EXCEL.xls";
    private static final String TWITTER = "https://twitter.com/";

    public void createExcel(List<String> twitterProfileUrls) {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Twitter Profile Excel");

        for (int i = 0; i < twitterProfileUrls.size(); i++) {
            Row row = sheet.createRow(i);
            Cell cell = row.createCell(0);
            cell.setCellValue(TWITTER + twitterProfileUrls.get(i));
        }

        try {
            FileOutputStream outputStream = new FileOutputStream(FILE_NAME);
            workbook.write(outputStream);
            workbook.close();
        } catch (Exception e) {
            Logger.getLogger(ExcelServiceImpl.class.getName(), e.getMessage());
        }
    }
}
