package com.company.search.engine.service;

import com.company.search.engine.model._TwitterProfile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by flooprea on 3/31/2018
 */

@Service
public interface TwitterProfileService {

    Optional<List<_TwitterProfile>> getTwitterProfilesForCompany(String companyName);

    _TwitterProfile indexTwitterProfile(_TwitterProfile twitterProfile);

    List<_TwitterProfile> indexTwitterProfiles(List<_TwitterProfile> twitterProfiles);

    Page<_TwitterProfile> getTwitterProfiles(Pageable pageable);
    List<_TwitterProfile> getTwitterProfiles();

    Optional<_TwitterProfile> getTwitterProfileById(String id);

    List<_TwitterProfile> getTwitterProfilesByCompanyId(String companyId);

    List<_TwitterProfile> getTwitterProfilesByCompanyName(String companyName);

    String  getCompanyIdByScreenName(String screenName);

    void openChrome(List<String> randomTwitterProfile);

    List<String>  getRandomTwitterProfiles(Integer totalNumber);

    void deleteTwitterProfiles();

    void deleteTwitterProfileById(String id);

    void deleteTwitterProfilesByCompanyId(String companyId);

    void deleteTwitterProfilesByCompanyName(String companyName);
}
