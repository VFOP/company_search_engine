package com.company.search.engine.model;

import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by flooprea on 3/29/2018
 */

@Document(indexName = "tweets-from-companies", type = "tweet")
public class _Tweet implements Serializable {
    private String id;
    private String twitterProfileId;
    private String text;
    private Date createdAt;
    private String fromUser;

    public _Tweet() {
    }

    public _Tweet(String id, String twitterProfileId, String text, Date createdAt, String fromUser) {
        this.id = id;
        this.twitterProfileId = twitterProfileId;
        this.text = text;
        this.createdAt = createdAt;
        this.fromUser = fromUser;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTwitterProfileId() {
        return twitterProfileId;
    }

    public void setTwitterProfileId(String twitterProfileId) {
        this.twitterProfileId = twitterProfileId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }
}
