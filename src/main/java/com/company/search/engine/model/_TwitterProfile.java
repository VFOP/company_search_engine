package com.company.search.engine.model;

import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;

/**
 * Created by flooprea on 3/29/2018
 */

@Document(indexName = "tweets-from-companies", type = "twitter-profile")
public class _TwitterProfile implements Serializable {
    private String id;
    private String companyId;
    private String screenName;
    private String name;
    private String description;
    private Boolean verified;

    public _TwitterProfile() {
    }

    public _TwitterProfile(String id, String companyId, String screenName, String name, String description, Boolean verified) {
        this.id = id;
        this.companyId = companyId;
        this.screenName = screenName;
        this.name = name;
        this.description = description;
        this.verified = verified;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }
}
