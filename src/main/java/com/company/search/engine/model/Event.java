package com.company.search.engine.model;

import java.util.Date;
import java.util.Objects;

/**
 * Created by flooprea on 5/6/2018
 */
public class Event {

    private String text;
    private Date createAt;
    private String createdBy;

    public Event() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Event)) return false;
        Event event = (Event) o;
        return Objects.equals(getText(), event.getText()) &&
                Objects.equals(getCreateAt(), event.getCreateAt()) &&
                Objects.equals(getCreatedBy(), event.getCreatedBy());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getText(), getCreateAt(), getCreatedBy());
    }
}
