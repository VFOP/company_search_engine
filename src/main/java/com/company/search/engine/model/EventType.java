package com.company.search.engine.model;

/**
 * Created by flooprea on 5/31/2018
 */
public enum EventType {

    NEW_OFFICE("new office"),
    LAUNCH_PRODUCT("launch product"),
    ATTEND_CONFERENCE("attend conference"),
    NEW_EMPLOYEE("new employee");

    EventType(String eventName) {
        this.eventName = eventName;
    }

    public final String eventName;

    public String getEventName() {
        return eventName;
    }
}
