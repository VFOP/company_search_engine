package com.company.search.engine.model;

import org.springframework.data.elasticsearch.annotations.Document;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by flooprea on 3/29/2018
 */
@Document(indexName = "tweets-from-companies", type = "company")
public class Company implements Serializable {
    private String id;
    private String name;
    private String uri;
    private Integer employeeNumber;
    private String type;
    private String country;
    private Map<EventType, List<_Tweet>> events;

    public Company() {
    }

    public Company(String id, String name, String uri, Integer employeeNumber, String type, String country) {
        this.id = id;
        this.name = name;
        this.uri = uri;
        this.employeeNumber = employeeNumber;
        this.type = type;
        this.country = country;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Integer getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(Integer employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Map<EventType, List<_Tweet>> getEvents() {
        return events;
    }

    public void setEvents(Map<EventType, List<_Tweet>> events) {
        this.events = events;
    }
}
