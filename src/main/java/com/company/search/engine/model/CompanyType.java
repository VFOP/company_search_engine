package com.company.search.engine.model;

/**
 * Created by flooprea on 6/1/2018
 */
public enum CompanyType {

    PUBLIC("Public company"),
    PRIVATE("Private company"),
    PUBLIC_LIMITED("Public limited company");

    CompanyType(String companyType) {
        this.companyType = companyType;
    }

    private final String companyType;

    public String getCompanyType() {
        return companyType;
    }
}
