SPARQL QUERY FOR COMPANIES 

PREFIX o: <http://dbpedia.org/ontology/>
PREFIX p: <http://dbpedia.org/property/>

SELECT ?uri ?numberOfEmployees

WHERE {
     ?uri a o:Company;
      
      o:numberOfEmployees ?numberOfEmployees;
 
      o:type ?type.

      FILTER regex(?type, "Public_company")      
}
ORDER BY DESC (?numberOfEmployees) 



PREFIX o: <http://dbpedia.org/ontology/>
PREFIX p: <http://dbpedia.org/property/>
PREFIX r: <http://dbpedia.org/resource/>

SELECT *

WHERE {
     ?uri a o:Company;
      o:numberOfEmployees ?numberOfEmployees;
      o:type ?type.
       FILTER (?type IN (r:Public_company, r:Private_Company, r:Public_limited_company))   
     }
ORDER BY DESC (?numberOfEmployees)


twitterProfile.getName().contains(companyName) ||
                        twitterProfile.getScreenName().contains(companyName))).forEach(returnedProfiles::add);*/
